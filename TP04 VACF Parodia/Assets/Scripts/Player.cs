using System.Collections;
using System.Collections.Generic;
using Unity.Burst.Intrinsics;
using UnityEngine;

public class Player : MonoBehaviour
{
    Rigidbody2D rb;
    [Header("Movimiento")]
    float movH;
    [SerializeField] float velMov;
    [Range(0,0.3f), SerializeField] float smoothMov;
    Vector3 velocity = Vector3.zero;
    bool lookRight = true;

    [Header("Salto")]
    [SerializeField] float forceJump;
    [SerializeField] LayerMask l_isFloor;
    [SerializeField] Transform floorController;
    [SerializeField] Vector3 boxDimension;
    bool b_isFloor;
    bool jump = false;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        movH = Input.GetAxisRaw("Horizontal") * velMov;

        if(Input.GetButtonDown("Jump"))
        {
            jump = true;
        }
    }

    void MovementLogic(float mov, bool jumping)
    {
        Vector3 velObj = new Vector2(mov, rb.velocity.y);
        rb.velocity = Vector3.SmoothDamp(rb.velocity, velObj, ref velocity, smoothMov);

        if(mov > 0 && !lookRight)
        {
            SwitchLook();
        } 
        else if (mov< 0  && lookRight)
        {
            SwitchLook();
        }

        if(b_isFloor && jump)
        {
            b_isFloor = false;
            rb.AddForce(new Vector2(0f, forceJump));
        }
    }

    void OnDrawGizmos() 
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(floorController.position, boxDimension);    
    }

    void SwitchLook()
    {
        lookRight = !lookRight;
        Vector3 m_scale = transform.localScale;
        m_scale.x *= -1;
        transform.localScale = m_scale;
    }

    void FixedUpdate() 
    {
        b_isFloor = Physics2D.OverlapBox(floorController.position, boxDimension, 0f, l_isFloor);
        MovementLogic(movH * Time.fixedDeltaTime, jump);
        jump = false;
    }
}
